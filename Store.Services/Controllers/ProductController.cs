﻿using Store.Models;
using Store.Services.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Store.Services.Controllers
{
    public class ProductController : ApiController
    {
        private readonly StoreDbContext db;
        public ProductController()
        {
            db = new StoreDbContext();
        }

        [HttpGet]
        public IHttpActionResult ListProducts()
        {
            var list = new List<Product>();
            list = db.Products.ToList();
            return Ok(list);
        }
        [HttpGet]
        public IHttpActionResult GetById(int Id)
        {
            var entity = db.Products.Find(Id);
            return Ok(entity);
        }

        [HttpPost]
        public IHttpActionResult ProductPost (int CatId, int ComId, string ModelNa, string Desc)
        {
            var entity = new Product()
            {
                CategoryId = CatId,
                BrandId = ComId,
                ModelName = ModelNa,
                Description = Desc
            };

            db.Products.Add(entity);
            db.SaveChanges();
            return Ok(entity);
        }


        [HttpGet]
        public IHttpActionResult GetByCountriesName()
        {
            var list = new List<Country>();
            list = db.Countries.ToList();
            return Ok(list);
        }
        [HttpGet]
        public IHttpActionResult GetByCategoriesName()
        {
            var list = new List<Category>();
            list = db.Categories.ToList();
            return Ok(list);
        }
        [HttpGet]
        public IHttpActionResult GetByCompaniesName()
        {
            var list = new List<Company>();
            list = db.Companies.ToList();
            return Ok(list);
        }
        
    }
}